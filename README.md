# LungCancerClassificationCNN



## I. Introduction:

This project is a Convolutional Neural Network that classifies whether someone has skin cancer or not. This network can classify 6 types of skin cancer which are: 

1. Actinic keratoses and intraepithelial carcinoma / Bowen's disease (akiec)
2. basal cell carcinoma (bcc)
3. dermatofibroma (df)
4. melanoma (mel)
5. melanocytic nevi (nv)
6. vascular lesions (angiomas, angiokeratomas, pyogenic granulomas and hemorrhage, vasc).

And benign lesions:
benign keratosis-like lesions (solar lentigines / seborrheic keratoses and lichen-planus like keratoses, bkl)


This model can detect the following kind of tumors with an overall accuracy of 99.37% on the train set, 91.51% on the validation set, and 90.21% on the test set and class accuracies on the test set as follows:
1. Healthy or normal tissue: 98.5%
2. Adenocarcinoma: 100%
3. Large cell carcinoma: 100%
4. Squamous cell carcinoma: 100%

1. Actinic keratoses and intraepithelial carcinoma / Bowen's disease (akiec): 100%
2. basal cell carcinoma (bcc): 100%
3. benign keratosis-like lesions (solar lentigines / seborrheic keratoses and lichen-planus like keratoses, bkl): 98.36%
4. dermatofibroma (df): 100%
5. melanoma (mel): 96.40%
6. melanocytic nevi (nv): 86.00%
7. vascular lesions (angiomas, angiokeratomas, pyogenic granulomas and hemorrhage, vasc): 100%

## II. Model Architecture:

This model consists of 3 convolutional layers and a fully-connected layer. The details of the model are as follows:

### 1. First Layer:
* Convolution operation with a kernel size of 3 by 3, a padding of 1, and a stride of 1.
* Batch normalization.
* Followed by max-pooling of kernel size of 3 by 3, and a stride of 3.
* Leaky ReLU activation with a negative slope of 0.1
* Dropout with a probability of 50%
This layer has 4 filters.

### 2. Second Layer:
* Convolution operation with a kernel size of 3 by 3, a padding of 0, and a stride of 1.
* Batch normalization.
* Followed by max-pooling of kernel size of 3 by 3, and a stride of 3.
* Leaky ReLU activation with a negative slope of 0.1
* Dropout with a probability of 50%
This layer has 8 filters.

### 3. Third Layer:
* Convolution operation with a kernel size of 5 by 5, a padding of 0, and a stride of 1.
* Batch normalization.
* Followed by max-pooling of kernel size of 3 by 3, and a stride of 3.
* Leaky ReLU activation with a negative slope of 0.1
* Dropout with a probability of 50%
This layer also has 16 filters.

### 4. The Fully-connected Layer:
In this layer, the output of the previous convolutional layer is flattened into a vector and is then fed to a linear layer. This layer has 7 outputs that correspond to all of the classes.

## NOTE: 
For the convolutional layers, all of the weights are initialized using the kaiming method and with biases equal to zero.

## III. Dataset:
This model was trained on the [Skin Cancer MNIST: HAM10000](https://www.kaggle.com/datasets/kmader/skin-cancer-mnist-ham10000) from Kaggle.
### Dataset details:
This dataset consists of 10015 skin images with moles or lesions.

### Data Description:
* All of the images of this dataset are of size 450 by 600
* The images are distributated as follows:
    1. akiec: 327 images.
    2. bcc: 514 images.
    3. bkl: 1099 images.
    4. df 115 images.
    5. mel: 1113 images.
    6. nv: 6705 images.
    7. vasc: 142.

### Data Limitation and Issues:
It is evident from the section above that the data is extremely imbalanced with the nv class having 6705 images and df class having only 115 images. This problem could produce a heavily skewed model that would perform greatly for the classes with more images and very poorly for the other classes.
This also caused the test and validation set to have very few images of some classes which might have produced some inaccurate results.
In conclusion, in order for the model to be reliable enough a different dataset or an updated version of this one (that does not have the acute class imbalance problem) needs to be used for training this model.

### Solutions:
As a solution for this issue a combination of undersampling and data augmentation was used on the training set. First of all, each class that had more than 3000 images (namely the nv class) was undersampled to have 3000 images. After this, all other classes were augmented to have roughly 3000 images each.

## IV. Training the model:
Training this model consists of 4 steps:
#### 1. Preprocessing the data:
The data was pre-processed before training. The pre-processing consists of:
1. Normalizing the dataset by finding the mean and standard deviation of the dataset and then subtracting the mean and dividing by the standard deviation.
2. Sharpening the images.

#### 2. Data augmentation:
Due to the training set being small, data augmentation was needed in order not to overfit the model.
The data augmentation was done by sampling images from the train split and applying some transformations to them.
The transformations were:
1. Random vertical flipping with a flipping probability of 75%
2. Random sharpness adjustment with a probability of 50%
3. Random gaussian blur with a filter size of 3 by 3 and sigma ranging from 0.1 - 1
4. Finally, Random auto contrast also with a probability of 50%
For training 5000 images were augmented (This value was obtained experimentally)
#### 3. Setting up the training routine:
The training routine consisted of the following:
#### A. Data loaders:
Three data loader (torch.utils.data.DataLoader) objects were created for each data split (train, validation, and test) with the splits being 80% for training 10% for validation and 10% for testing with a number of workers of 8 and a batch size of 32 for the training set and 16 for the validation and test sets.
#### B. The Optimizer:
The optimizer is Adam with:
* Initial learning rate of 1e-3
* Weight decay of 5e-7
This optimizer was accompanied by a learning rate scheduler which is StepLR with a step size of 17 and a gamma value of 0.1
#### C. The Loss Function:
The Cross Entropy Loss function was used.
#### D. Epochs:
The model was trained for 25 epochs.

## V. Results:
After training for 30 epochs the model achieved train accuracy of 98.01%, validation accuracy of 92.02% and a test accuracy of 99.47%.

# IMPROTANT NOTE: 
* It is important to include a .txt file in the data folder with the name "paths.txt" which contains the path to the data.
The file format should be as follows:
<pre><code>{
    "data_path" : "[The path of the data]"
    "labels": "[The path of labels]",
    "aug_data": "[The path of the augmented data]",
}
</pre></code>
* For figures of the train, validation accuracies and the loss behavior refer to the figures in the main_29_10_1.ipynb file.
