# PyTorch imports:
import torch
import torch.nn as nn



class Model(nn.Module):
    def __init__(self, num_classes, image_dim, in_channels=3, dropout_prob=[0.5] * 3):
        super(Model, self).__init__()


        # --- The activation function: ----------------------------------------
        self.leaky_relu = nn.LeakyReLU(negative_slope=0.1)

        # =====================================================================
        # --- Convolutional Layers: -------------------------------------------
        #
        # --- First Layer: ----------------------------------------------------
        out_channels_1 = 64
        kernel_size, padding, stride = 3, 1, 1
        conv1_params = {'in_channels' : in_channels, 'out_channels' : out_channels_1,
                        'kernel_size' : kernel_size, 'stride' : stride, 'padding' : padding}
        kernel_size, padding, stride = 3, 0, 3
        max_pool1_params = {'kernel_size' : kernel_size, 'padding' : padding, 'stride' : stride}

        # --- Second Layer: ----------------------------------------------------
        out_channels_2 = 64
        kernel_size, padding, stride = 3, 1, 1
        conv2_params = {'in_channels' : out_channels_1, 'out_channels' : out_channels_2,
                        'kernel_size' : kernel_size, 'stride' : stride, 'padding' : padding}
        kernel_size, padding, stride = 3, 0, 3
        max_pool2_params = {'kernel_size' : kernel_size, 'stride' : stride, 'padding' : padding}

        # --- Third Layer: ----------------------------------------------------
        out_channels_3 = 128
        kernel_size, padding, stride = 3, 1, 1
        conv3_params = {'in_channels' : out_channels_2, 'out_channels' : out_channels_3,
                        'kernel_size' : kernel_size, 'stride' : stride, 'padding' : padding}
        kernel_size, padding, stride = 3, 0, 3
        max_pool3_params = {'kernel_size' : kernel_size, 'stride' : stride, 'padding' : padding}

        modules = [
            # First Layer: -----------------------
            nn.Sequential(
                nn.Conv2d(**conv1_params),
                nn.BatchNorm2d(out_channels_1),
                nn.MaxPool2d(**max_pool1_params),
                nn.Dropout(p=dropout_prob[0])
                        ),
            # Second Layer: ----------------------
            nn.Sequential(
                nn.Conv2d(**conv2_params),
                nn.BatchNorm2d(out_channels_2),
                nn.MaxPool2d(**max_pool2_params),
                nn.Dropout(p=dropout_prob[1])
                        ),
            # Third Layer: -----------------------
            nn.Sequential(
                nn.Conv2d(**conv3_params),
                nn.BatchNorm2d(out_channels_3),
                nn.MaxPool2d(**max_pool3_params),
                nn.Dropout(p=dropout_prob[2])
                        )
                ]

        # Creating the module list that holds all the layers except for
        # the fully connected layer:
        self.module_list = nn.ModuleList(modules=modules)

        # Calculating how the dimension of the image would change when applying
        # the convolution and max pooling operations:
        new_image_dim = image_dim

        # Iterating over the layers:
        for layer in self.module_list:
            # Iterating over the operations in the layers:
            for op in layer:
                if isinstance(op, nn.Conv2d) or isinstance(op, nn.MaxPool2d):
                    new_image_dim = self.new_image_dim(new_image_dim, op.kernel_size,
                                                  op.padding, op.stride)

        # --- The fully-connected layer: --------------------------------------------------
        in_features = new_image_dim[0] * new_image_dim[1] * out_channels_3
        self.fc = nn.Linear(in_features=in_features, out_features=num_classes)
        
        # In the below loop 3 main operations are done:
        #   1) Initializing all the weights of the convolutional layers.
        #   2) Initializing all the biases of the convolutional layers.
        #   3) Adding all the convolutional layers to an array so they can
        #       be used for visualizing and analysis.
        self.convs = []
            
        # Iterating over the layers:
        for layer in self.module_list:
            # Iterating over the operations in the layers:
            for op in layer:
                if isinstance(op, nn.Conv2d):
                    self.convs.append(op)
                    nn.init.kaiming_normal_(op.weight)
                    nn.init.constant_(op.bias, 0.0)

    
    def forward(self, x):
        
        # --- The convolutional layers: ------------------------
        for layer in self.module_list:
            for op in layer:
                x = op(x)
        
        # Flattening the feature maps so they can fit the fully-connected layer:
        x = x.view(x.size(0), -1)
        
        # --- The fully-connected layer: -----------------------
        x = self.fc(x)
        
        return x
    
    
    def new_image_dim(self, image_dim : tuple, kernel_size : tuple, padding, stride):
        """ Finds the new dimensions of an input feature map after applying
        convolution or pooling.

        Args:
            image_dim (tuple): A tuple containing the dimension of the input 
            feature map.
            kernel_size (int): The kernel size of the filter.
            padding (int/tuple): The amount of padding to the feature map.
            stride (int/tuple): The stride (step size) of the filter.
        """
        if isinstance(padding, int):
            padding_height = padding_width = padding
        else:
            padding_height, padding_width = padding
            
        if isinstance(stride, int):
            stride_height = stride_width = stride
        else:
            stride_height, stride_width = stride

        if isinstance(kernel_size, int):
            kernel_height = kernel_width = kernel_size
        else:
            kernel_height, kernel_width = kernel_size
            
        new_image_height = ((image_dim[0] + 2 * padding_height - kernel_height) // stride_height) + 1
        new_image_width = ((image_dim[1] + 2 * padding_width - kernel_width) // stride_width) + 1
        
        return (new_image_height, new_image_width)
