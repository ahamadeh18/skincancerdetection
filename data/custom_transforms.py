# This file contains custom transforms that are
# not available in trochvision.transforms.

from torchvision import transforms as T


class Sharpen():
    """Sharpens an image (torch.Tensor) according to
    a given sharpness factor sharpness_factor.
    """
    def __init__(self, sharpness_factor=2):
        self.sharpness_factor = sharpness_factor
        
    def __call__(self, image):
        
        image = T.functional.adjust_sharpness(image, self.sharpness_factor)
        return image