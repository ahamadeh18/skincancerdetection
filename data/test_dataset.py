from dataset_alt import *

import numpy as np
import random

import cv2


indecies = np.arange(10)

dataset = SkinDataset(indecies=indecies)

data_dist = dataset.get_data_distribution()
aug_amount = dataset.find_balancing_aug_amount()

# print(dataset.mapped_labels)
aug_dataset = SkinDataset(indecies=indecies, augment=True, aug_amount=aug_amount)
for image, y, label, id in aug_dataset:
    image = np.transpose(image.cpu().detach().numpy(), (1, 2, 0))
    if id[0] == 'A':
        cv2.imshow(f'ID: {id} | {label}', image)
        cv2.waitKey(0)

cv2.destroyAllWindows()