# PyTorch imports:
import torch
from torch.utils.data import Dataset
import torchvision.transforms as T

# NumPy imports:
import numpy as np

# Pandas imports:
import pandas as pd

# OpenCV imports:
import cv2

# Glob imports:
import glob

# Other imports:
import os
import json
import random

# --- Constants and default values: -----------------------------------------------------------------

dataset_json_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'dataset_location.txt')
with open(dataset_json_path, 'r') as file:
    data_dict = json.loads(file.read())
    DEFAULT_DATASET_PATH = data_dict['data']
    DEFAULT_METADATA_PATH = data_dict['labels']


data_stats_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'dataset_stats.txt')

with open(data_stats_path, 'r') as file:
    stats_dict = json.loads(file.read())
    MEAN, STD = stats_dict['mean'], stats_dict['std']


ORIGINAL_IMAGE_DIM = (450, 600)
RESIZED_IMAGE_DIM = (112, 150)
fraction = 0.9
CROPPED_IMAGE_DIM = (int(RESIZED_IMAGE_DIM[0] * fraction), int(RESIZED_IMAGE_DIM[1] * fraction))
DEFAULT_TRANSFORM = T.Compose(
    [
        T.ToTensor()
    ])

DEFAULT_AUG_TRANSFORMS = T.Compose(
    [   
        T.ToTensor(),      
        T.RandomHorizontalFlip(p=0.5),
        T.RandomVerticalFlip(p=0.5),
        T.RandomGrayscale(p=0.2),
        T.RandomAdjustSharpness(sharpness_factor=1.0, p=0.5),
        T.RandomAutocontrast(p=0.35),        
        T.RandomApply([T.RandomRotation(degrees=90),
                        T.GaussianBlur(kernel_size=3, sigma=(0.1, 0.2)),
                        T.ColorJitter(brightness=0.5, hue=0.3, saturation=0.5),
                        T.RandomCrop(CROPPED_IMAGE_DIM, pad_if_needed=True)], p=0.30)

    ]    
)

# --- The Dataset class: ----------------------------------------------------------------------------

class SkinDataset(Dataset):
    def __init__(self, dataset_path=DEFAULT_DATASET_PATH, metadata_path=DEFAULT_METADATA_PATH,
                 transform=DEFAULT_TRANSFORM, indecies=None, augment=False, aug_amount=1000,
                 aug_transform=DEFAULT_AUG_TRANSFORMS):
                
        # Setting the dataset and metadata paths:
        self.dataset_path = dataset_path
        self.metadata_path = metadata_path
        
        # Setting the image transforms:
        self.transforms = transform
        
        # Setting the indecies. This variable helps define
        # The train, validation, test splits.
        self.indecies = indecies
        
        # Getting the paths of the images:
        self.image_paths = self.get_image_paths()
        
        # Getting the IDs of the images:
        self.image_ids = self.get_image_ids()

        # Loading the metadata:
        self.metadata = self.load_metadata()
        
        # Loading the labels, the mapped labels and the mapping criteria:
        self.labels, self.mapped_labels, self.mapping, self.rev_mapping = self.load_labels()
        
        self.classes = np.unique(self.labels)
        self.num_classes = len(self.classes)
        
        # --- Data Augmentation: ------------------------------------------------
        self.augment = augment
        self.aug_amount = aug_amount
        
        self.aug_probability = None
        
        if augment:
            # Setting the augmentation criteria:
            self.aug_transforms = aug_transform
                
            # Getting the augmentation probabilities for all the classes:
            self.aug_probability = self.augment_class_probability()
            
            # Getting the augmentation paths, labels, mapped labels, and ids:
            aug_paths, aug_labels, mapped_aug_labels, aug_ids = self.augment_data()
            
            # Updating the class variables with the augmented data.
            self.image_paths = np.append(self.image_paths, aug_paths)
            self.labels = np.append(self.labels, aug_labels)
            self.mapped_labels = np.append(self.mapped_labels, mapped_aug_labels)
            self.image_ids = np.append(self.image_ids, aug_ids)
        
    
    def get_image_paths(self):
        """ Loads the paths of the images of the dataset.

        Returns:
            np.ndarray: An array containing all of the paths
                to the images of the dataset.
        """
        # Getting the paths while providing the image type to be jpg:
        image_paths = np.array(glob.glob(os.path.join(self.dataset_path, '*.jpg')))
        
        # Taking the paths that correspond to the self.indecies:
        if self.indecies is not None:
            image_paths = image_paths[self.indecies]
        
        return image_paths
    
    def get_image_ids(self):
        """ Gets the image IDs from the names of the images that are
        located in the self.dataset_path variable

        Returns:
            np.ndarray: A numpy array containing the image ids ordered
                as they are ordered in the files they were retireved
                from.
        """
        # Getting a list that contains the images' names:
        image_names = os.listdir(self.dataset_path)
        
        # Removing the file extention from the image names to get the IDs
        # The image names are constructed as follows: [Image ID].jpg so 
        # removing the image type/extention would leave the image ID only.
        image_ids = np.array([image_name.split('.')[0] for image_name in image_names])
        
        # Taking the image ids that correspond to the self.indecies:
        # if self.indecies is not None:
        #     image_ids = image_ids[self.indecies]
            
        return image_ids

    
    def load_metadata(self):
        """ Loads the metadata and align it with the order of the image
        IDs.

        Returns:
            pd.DataFrame: A DataFrame containing the metadata of the 
                dataset aligned with the image ids.
        """
    
        # Loading the metadata into a pd.DataFrame:
        metadata = pd.read_csv(self.metadata_path)
        
        # Aligning the metadata entries with image_ids using the id column:
        temp_df = pd.DataFrame(np.copy(self.image_ids), columns=['image_id'])
        metadata = pd.merge(temp_df, metadata, on='image_id')
        
        return metadata
      
    
    def load_labels(self):
        """ Loads the labels from the metadata DataFrame and maps
        the labels to numerical values.

        Returns:
            labels: The labels of the images of the dataset.
            mapped_labels: The labls of the images of the
                dataset mapped to integer values.
            mapping: The mapping criteria.
            rev_mapping: The reverse of the mapping criteria.
        """
        
        # --- Getting the labels from the metadata DataFrame: -----------
        labels = np.array(self.metadata.dx)
        
        # Taking the labels that correspond to the self.indecies:
        if self.indecies is not None:
            labels = labels[self.indecies]
        
        # --- Creating the mapping and the reverse of this mapping: -----
        # Getting the unique labels:
        unique_labels = np.unique(labels)
        # Creating the mapping:
        mapping = dict(zip(unique_labels, np.arange(len(unique_labels))))
        # Creating the reverse of the mapping:
        rev_mapping = dict(zip(mapping.values(), mapping.keys()))
        
        # --- Creating the mapped labels: --------------------------------
        mapped_labels = np.array([mapping[label] for label in labels])
        
        return labels, mapped_labels, mapping, rev_mapping
    
    
    def get_class_indecies(self, cls):
        """ Finds the image indecies of a given class

        Returns:
            np.ndarray: An array containing the indecies
                of a given class
        """
        # Mapping the class name if it was given as a string:
        if isinstance(cls, str):
            cls = self.mapping[cls]
        
        # Getting the indecies that correspond to the given class:        
        indecies = [idx for idx, l in enumerate(self.mapped_labels) if l == cls]
        
        return indecies    
    
    def get_data_distribution(self):
        """ Calculated the distribution of the dataset, i.e., how many
        images each class has.

        Returns:
            dict: A dictionary with keys as class names and values as
                the number of images this class has. 
        """
        data_dist = np.bincount(self.mapped_labels)
        data_dist = dict(zip(self.classes, data_dist))
        
        return data_dist
    
    
    def find_balancing_aug_amount(self, extra=0):
        """ Calculates the number of data augmentation needed for each
        class so that all the classes are balanced.

        Args:
            extra (int, optional): An constant amount to add to all of 
            the classes. Defaults to 0.

        Returns:
            dict: A dictionary with keys as class names and values as
                the augmentation amount needed. 
        """
        
        # Getting the maximum amount of images a single class contains:
        data_dist = self.get_data_distribution()
        max_amount = max(list(data_dist.values()))
        
        # Calculating the amount of images needed for all the classes
        # so that they have the same amount of images as the maximum
        # class.
        aug_amount_needed = np.abs(np.array(list(data_dist.values())) - max_amount)
        aug_amount_needed = dict(zip(self.classes, aug_amount_needed + extra))
        
        return aug_amount_needed
    
    # --- Augmentation functions: ---------------------------------------------------
    def augment_data(self):
        """ Augments the data for each class by the amount given by the 
        self.aug_amount variable. The augmentation consists of taking random
        sample from the image paths and duplicating them then augmenting them
        on-demand when requested by the __getitem__ method.
        
        An image is augmented with a propability p_c where:
                p_c is the probabilty to augment an image from class c
        and p_c for a given class is:
                            1 - n_c / (n_c + aug_c)
            where n_c is the number of images in class c, and aug_c
            is the number of augmented images in a class c.

        Returns:
            aug_paths: An array containing the paths of the augmented images.
            aug_labels: An array containing the labels to the augmented images.
            mapped_aug_labels: An array containing the mapped labels to augmented
                images.
            aug_ids: An array containing the image ids of the augmetned    
                images.
        """
        if isinstance(self.aug_amount, int):
            # If the augmentation amount is an integer, then a dictionary
            # of the form {"class_name" : number augmentation} is constructed.
            self.aug_amount = [self.aug_amount] * self.num_classes
            self.aug_amount = dict(zip(self.unique(self.labels), self.aug_amount))
        
        aug_paths = np.array([])
        aug_labels = np.array([])
        mapped_aug_labels = np.array([])
        aug_ids = np.array([])
        
        for cls, amount in self.aug_amount.items():
            class_indecies = self.get_class_indecies(cls)
            to_augment = np.random.choice(class_indecies, amount)
            
            aug_paths = np.append(aug_paths, np.copy(self.image_paths[to_augment]))
            aug_labels = np.append(aug_labels, np.copy(self.labels[to_augment]))
            mapped_aug_labels = np.append(mapped_aug_labels, np.copy(self.mapped_labels[to_augment])).astype(int)
            aug_ids = np.append(aug_ids, np.copy(self.image_ids[to_augment]))
            
        return aug_paths, aug_labels, mapped_aug_labels, aug_ids
    
    def augment_class_probability(self):
        """ Calculates the probability (p_c) of augmenting a class for all the
        classes, where:
                p_c = 1 - n_c / (n_c + aug_c)
                n_c = number of images in class c
                aug_c number of augmented images in class c

        Returns:
            _type_: _description_
        """
        
        if isinstance(self.aug_amount, int):
            self.aug_amount = dict(zip(self.classes, [self.aug_amount] * self.num_classes))
        
        augment_probabilites = {}
        for c, aug_c in self.aug_amount.items():
            # Calculating the probability of augmenting class c:
            n_c = len(self.get_class_indecies(c))
            p_c = 1 - n_c / (n_c + aug_c)
            
            augment_probabilites[c] = p_c
            
        return augment_probabilites
    
    # --- Overriding magic methods: --------------------------------------------------------
    
    def __len__(self):
        return len(self.labels)
    
    
    def __getitem__(self, index):
        
        # Getting the image path:
        image_path = self.image_paths[index]
        # Loading the image:
        image = cv2.imread(image_path)
        # Getting the original and mapped labels:
        label = self.labels[index]
        mapped_label = self.mapped_labels[index]
        
        # Getting the image id:
        image_id = self.image_ids[index]
        
        if self.augment:
            # Augmenting an image with probability p_c
            # Where p_c = 1 - n_c / (n_c + aug_c); 
            # n_c: number of images in class c.
            # aug_c: number of augmented images in class c.    
            aug_prob = self.aug_probability[label]
            
            rand = random.random()
            if rand < aug_prob:
                image = self.aug_transforms(image)
                image_id = f'AUG_{image_id}'
                image = np.transpose(image.cpu().detach().numpy(), (1, 2, 0))
                
        # Applying the transforms:
        image = self.transforms(image)
        
        # return image, mapped_label, label, image_id
        return image, mapped_label


