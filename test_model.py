from model_alt import *
from data.dataset import *
import numpy as np
import random
import cv2

from torch.utils.data import WeightedRandomSampler, DataLoader


dataset = SkinCancerDataset()
indecies = np.random.choice(np.arange(len(dataset)), 2000)

dataset = SkinCancerDataset(indecies=indecies)

# Calculating the weights of each class:
weights = np.array(list(dataset.get_data_distribution().values()))
weights = 1 / weights

# Assiging the weights to the images:
image_weights = [weights[i] for i in dataset.mapped_labels]

sampler = WeightedRandomSampler(weights=image_weights, num_samples=len(dataset), replacement=True)

dataloader = DataLoader(dataset=dataset, batch_size=40, shuffle=True, pin_memory=True)

# classes = {0 : [], 1 : [], 2 : [], 3 : [], 4 : [], 5 : [], 6 : []}

classes = np.array([0, 0, 0, 0, 0, 0, 0])

for x, y in dataloader:
    y = y.cpu().detach().numpy()
    arr = np.zeros_like(classes)
    for y_i in y:
        arr[y_i] += 1
    print(arr)
    classes += arr
    
print(classes)



# indecies = np.arange(10)
# dataset = SkinDataset(indecies=indecies)


# index = random.choice(indecies)

# image = dataset[index][0]

# image = image.view(1, *image.shape)

# model = Model(image_dim=(270, 360), num_classes=7)


# pred = model(image)

# print(pred.shape)


